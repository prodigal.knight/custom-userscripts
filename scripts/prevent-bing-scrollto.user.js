// ==UserScript==
// @name         Prevent Bing scrollTo
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Prevent Bing search results page from scrolling results back to the top after a period of inactivity
// @author       You
// @match        https://www.bing.com/search*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=bing.com
// @grant        none
// @run-at       document-start
// ==/UserScript==

(function() {
  'use strict';

  const nop = () => {};

  let origScrollTo;

  window.addEventListener('focus', () => {
    setTimeout(() => { window.scrollTo = origScrollTo; }, 50);
  });

  window.addEventListener('blur', () => {
    origScrollTo = window.scrollTo;
    window.scrollTo = nop;
  });
})();
