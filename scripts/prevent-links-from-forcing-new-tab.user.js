// ==UserScript==
// @name         Prevent links from forcing new tab
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        *://*/*
// @grant        none
// @run-at       document-start
// ==/UserScript==

(function() {
  'use strict';

  setInterval(() => {
    const links = document.evaluate('//a[@target = "_blank"]', document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE);
    const l = links.snapshotLength;
    for (let i = 0; i < l; i++) {
      const link = links.snapshotItem(i);

      link.removeAttribute('target');
    }
  }, 50);
})();
