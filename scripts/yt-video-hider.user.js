// ==UserScript==
// @name         Hide YouTube Videos
// @description  Hide the YouTube video thumbnails you don't want to see on your Subscriptions page
// @namespace    http://tampermonkey.net/
// @version      0.1.1
// @author       You
// @match        https://www.youtube.com/feed/subscriptions
// @match        https://www.youtube.com/@*
// @icon         http://youtube.com/favicon.ico
// @grant        none
// @run-at       document-start
// ==/UserScript==

(function() {
  'use strict';

  const isSubscriptionsPage = document.URL.endsWith('/subscriptions');

  const normalizedString = selector => `normalize-space(string(${selector}))`;

  const thumbnailSelector = 'ytd-rich-item-renderer';
  const thumbnail = filter => `//${thumbnailSelector}[not(contains(@style, "display: none")) and (${filter})]`;

  const metadataText = normalizedString('.//*[@id = "metadata-line"]');
  const videoTitle = normalizedString('.//*[@id = "video-title"]');

  const isShort = './/*[@overlay-style = "SHORTS"]';
  const isLive = 'contains(.//yt-icon/span/text(), "LIVE")';
  const isStream = `contains(${metadataText}, "Stream")`;

  const or = (...conditions) => `(${[].concat(...conditions).join(' or ')})`;
  const and = (...conditions) => `(${[].concat(...conditions).join(' and ')})`;

  const hasChannelTag = (...tags) => or(tags.map(tag => `.//*[@id = "channel-name"]//a[@href = "/@${tag}"]`));
  const channelTagContains = (...tags) => or(tags.map(tag => `.//*[@id = "channel-name"]//a[contains(@href, "${tag}")]`));
  const channelTagStartsWith = (...tags) => or(tags.map(tag => `.//*[@id = "channel-name"]//a[starts-with(@href, "@/${tag}")]`));

  const asmrVtubers = ['NadeKomaKemomimiRefle', 'necomakarin', 'RengeKitutuki', 'Tsukimachi_nyamo'];

  const tests = [
    `${hasChannelTag('theescapist')} and not(contains(${videoTitle}, "Punctuation"))`,
    `${hasChannelTag('absoluteunit30', 'ConservativeTwins')} and (${isStream} or ${isLive})`,
    `${hasChannelTag(...asmrVtubers)} and ${isStream} and not(contains(${videoTitle}, "ASMR"))`,
    `${hasChannelTag('Timcast', 'TimcastNews', 'ConservativeTwins', 'ASMRBakery', 'AwakenWithJP')} and ${isShort}`,
    `${hasChannelTag('Timcast')} and not(starts-with(${videoTitle}, "The Culture War"))`,
    `${hasChannelTag('theblackpantslegion')} and not(starts-with(${videoTitle}, "Tex Talks"))`,
    `${hasChannelTag('Dogen')} and contains(${videoTitle}, "Lecture")`,
    `${hasChannelTag('S2Underground')} and not(contains(${videoTitle}, "Intel Update"))`,
    `${hasChannelTag('Tsukimachi_nyamo')} and contains(${videoTitle}, "パチンコ")`,
    `${hasChannelTag('ConservativeTwins')} and contains(${videoTitle}, "Giveaway")`,
    `${hasChannelTag('absoluteunit30')} and contains(${videoTitle}, "iLadies")`,
    `${hasChannelTag('SolusAstorias')} and contains(${videoTitle}, "Tyrone Stash")`,
    `${hasChannelTag('NadeKomaKemomimiRefle')} and contains(${videoTitle}, "ピクミン")`,
  ]
    .concat(
      isSubscriptionsPage ? ['years', 'year', 'months', 'month'].map(duration => `contains(${metadataText}, "${duration} ago") and ${isStream}`) : undefined,
      ['Premieres', 'Scheduled'].map(start => `contains(${metadataText}, "${start}")`),
    )
    .filter(Boolean)
    .map(thumbnail)
    .join('|')
    ;

  function hideThumbnails() {
    const container = document.querySelector('ytd-browse');
    if (!container) {
      return Infinity;
    }
    const r = document.evaluate(tests, container, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE);
    const n = r.snapshotLength;
    if (n) console.log('Hiding', n, 'video thumbnails');
    for (let i = 0; i < n; i++) {
      r.snapshotItem(i).style.display = 'none';
    }
    return n;
  }

  function prepToHideThumbnails() {
    let count = 0;
    let interval = setInterval(() => { if (hideThumbnails() === 0) { if (count++ === 2) { clearInterval(interval); } } }, 100);
  }

  document.addEventListener('scroll', prepToHideThumbnails);
  document.addEventListener('DOMContentLoaded', prepToHideThumbnails);
  document.addEventListener('visibilitychange', prepToHideThumbnails);
})();
